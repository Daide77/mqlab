package main

import (
	"fmt"
	"log"

	"github.com/ibm-messaging/mq-golang-jms20/mqjms"
)

func main() {
	fmt.Println("Entering main!!!")

	cf := mqjms.ConnectionFactoryImpl{
		QMName:           "DAVEMQA",
		Hostname:         "ibm-mq9.2a",
		PortNumber:       1414,
		ChannelName:      "DEV.APP.SVRCONN",
		TLSCipherSpec:    "ANY_TLS12",
		TLSClientAuth:    mqjms.TLSClientAuth_REQUIRED,
		KeyRepository:    "/var/mqm/qmgrs/ssl/mq_golang_client1/mq_golang_client1",
		CertificateLabel: "ibmwebspheremqmq_golang_client1",
		UserName:         "app",
		Password:         "ciaociao",
	}

	context, errCtx := cf.CreateContext()
	if context != nil {
		defer context.Close()
	}

	if errCtx != nil {
		log.Fatal(errCtx)
	} else {
		fmt.Println("no errors!!!")
	}

	// https://github.com/ibm-messaging/mq-golang-jms20
	// Create a Queue object that points at an IBM MQ queue
	queue := context.CreateQueue("RQ.APP2")
	queueGet := context.CreateQueue("DEV.QUEUE.2")

	// Send a message to the queue that contains the specified text string
	context.CreateProducer().SendString(queue, "My first message")
	context.CreateProducer().SendString(queue, "My second message")
	context.CreateProducer().SendString(queue, "My thrid message")
	context.CreateProducer().SendString(queue, "My fourth message")

	// Create a consumer, using Defer to make sure it gets closed at the end of the method
	consumer, conErr := context.CreateConsumer(queueGet)
	if conErr != nil {
		// Handle error here!
	}
	if consumer != nil {
		defer consumer.Close()
	}

	// Receive a message from the queue and return the string from the message body
	rcvBody, err := consumer.ReceiveStringBodyNoWait()

	if err == nil && rcvBody != nil {
		fmt.Println("Received text string: " + *rcvBody)
	} else if err != nil {
		fmt.Println("Error:")
		log.Fatal(err)
	} else {
		fmt.Println("No message received")
	}

	fmt.Println("program ends!!!")
}
