package main

import (
	"encoding/hex"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/ibm-messaging/mq-golang/v5/ibmmq"
)

func main() {
	var qMgrName string
	var qName string
	var err error
	var qMgr ibmmq.MQQueueManager
	var rc int

	var qObject ibmmq.MQObject
	var qObjectPut ibmmq.MQObject
	var qNamePut string

	// Which queue manager do we want to connect to
	qMgrName = "DAVEMQB"
	qNamePut = "RQ.APP1"
	qName = "DEV.QUEUE.2"

	// Allocate the MQCNO and MQCD structures needed for the CONNX call.
	cno := ibmmq.NewMQCNO()
	cd := ibmmq.NewMQCD()

	// Fill in required fields in the MQCD channel definition structure
	cd.ChannelName = "DEV.APP.SVRCONN"
	cd.ConnectionName = "ibm-mq9.2b(1414)"

	// Reference the CD structure from the CNO and indicate that we definitely want to
	// use the client connection method.
	cno.ClientConn = cd
	cno.Options = ibmmq.MQCNO_CLIENT_BINDING

	// MQ V9.1.2 allows applications to specify their own names. This is ignored
	// by older levels of the MQ libraries.
	cno.ApplName = "Golang ApplName"

	// Also fill in the userid and password if the MQSAMP_USER_ID
	// environment variable is set. This is the same variable used by the C
	// sample programs such as amqsput shipped with the MQ product.
	userId := "app"
	csp := ibmmq.NewMQCSP()
	csp.AuthenticationType = ibmmq.MQCSP_AUTH_USER_ID_AND_PWD
	csp.Password = "ciaociao"
	csp.UserId = userId
	// Make the CNO refer to the CSP structure so it gets used during the connection
	cno.SecurityParms = csp

	// And now we can try to connect. Wait a short time before disconnecting.
	qMgr, err = ibmmq.Connx(qMgrName, cno)
	if err == nil {
		fmt.Printf("Connection to %s succeeded.\n", qMgrName)
		// d, _ := time.ParseDuration("3s")
		// time.Sleep(d)
		// add
		// Create the Object Descriptor that allows us to give the queue name
		mqod := ibmmq.NewMQOD()

		// We have to say how we are going to use this queue. In this case, to GET
		// messages. That is done in the openOptions parameter.
		openOptions := ibmmq.MQOO_INPUT_EXCLUSIVE

		// Opening a QUEUE (rather than a Topic or other object type) and give the name
		mqod.ObjectType = ibmmq.MQOT_Q
		mqod.ObjectName = qName
		qObject, err = qMgr.Open(mqod, openOptions)
		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Opened queue", qObject.Name)
			defer close(qObject)
		}

		//PUT
		mqodPut := ibmmq.NewMQOD()
		openOptionsPut := ibmmq.MQOO_OUTPUT
		mqodPut.ObjectType = ibmmq.MQOT_Q
		mqodPut.ObjectName = qNamePut
		qObjectPut, err = qMgr.Open(mqodPut, openOptionsPut)
		if err != nil {
			fmt.Println("Opened queue for put failed")
			fmt.Println(err)
			// TODO EXIT
		} else {
			// Make sure we close the queue once we're done with it
			fmt.Println("Opened queue", qObjectPut.Name)
			defer close(qObject)
		}
		// The PUT requires control structures, the Message Descriptor (MQMD)
		// and Put Options (MQPMO). Create those with default values.
		putmqmd := ibmmq.NewMQMD()
		pmoPut := ibmmq.NewMQPMO()

		// The default options are OK, but it's always
		// a good idea to be explicit about transactional boundaries as
		// not all platforms behave the same way.
		pmoPut.Options = ibmmq.MQPMO_NO_SYNCPOINT

		// Tell MQ what the message body format is. In this case, a text string
		putmqmd.Format = ibmmq.MQFMT_STRING

		// And create the contents to include a timestamp just to prove when it was created
		msgData := "Hello from client2 in Go at " + time.Now().Format(time.RFC3339)

		// The message is always sent as bytes, so has to be converted before the PUT.
		buffer := []byte(msgData)

		// Now put the message to the queue
		err = qObjectPut.Put(putmqmd, pmoPut, buffer)

		if err != nil {
			fmt.Println(err)
		} else {
			fmt.Println("Put message to", strings.TrimSpace(qObjectPut.Name))
			// Print the MsgId so it can be used as a parameter to amqsget
			fmt.Println("MsgId:" + hex.EncodeToString(putmqmd.MsgId))
		}
		// END PUT

		msgAvail := true
		for msgAvail == true && err == nil {

			var datalen int
			// The GET requires control structures, the Message Descriptor (MQMD)
			// and Get Options (MQGMO). Create those with default values.
			getmqmd := ibmmq.NewMQMD()
			gmo := ibmmq.NewMQGMO()

			// The default options are OK, but it's always
			// a good idea to be explicit about transactional boundaries as
			// not all platforms behave the same way.
			gmo.Options = ibmmq.MQGMO_NO_SYNCPOINT

			// Set options to wait for a maximum of 3 seconds for any new message to arrive
			gmo.Options |= ibmmq.MQGMO_WAIT
			gmo.WaitInterval = 3 * 1000 // The WaitInterval is in milliseconds

			// There are now two forms of the Get verb.
			// The original Get() takes
			// a buffer and returns the length of the message. The user can then
			// use a slice operation to extract just the relevant data.
			//
			// The new GetSlice() returns the message data pre-sliced as an extra
			// return value.
			//
			// This boolean just determines which Get variation is demonstrated in the sample
			useGetSlice := true
			if useGetSlice {
				// Create a buffer for the message data. This one is large enough
				// for the messages put by the amqsput sample. Note that in this case
				// the make() operation is just allocating space - len(buffer)==0 initially.
				buffer := make([]byte, 0, 1024)
				// buffer := make([]byte, 0, 10)  MQGET: MQCC = MQCC_WARNING [1] MQRC = MQRC_TRUNCATED_MSG_FAILED [2080]
				// var buffer []byte   MQGET: MQCC = MQCC_WARNING [1] MQRC = MQRC_TRUNCATED_MSG_FAILED [2080]

				// Now we can try to get the message. This operation returns
				// a buffer that can be used directly.
				buffer, datalen, err = qObject.GetSlice(getmqmd, gmo, buffer)

				if err != nil {
					msgAvail = false
					fmt.Println(err)
					mqret := err.(*ibmmq.MQReturn)
					if mqret.MQRC == ibmmq.MQRC_NO_MSG_AVAILABLE {
						// If there's no message available, then I won't treat that as a real error as
						// it's an expected situation
						rc = int(mqret.MQRC)
						err = nil
					}
				} else {
					// Assume the message is a printable string, which it will be
					// if it's been created by the amqsput program
					fmt.Println("Put message to", strings.TrimSpace(qObject.Name))
					fmt.Println("MsgId:" + hex.EncodeToString(getmqmd.MsgId))
					// len of the payload
					fmt.Printf("Got message of length %d: ", datalen)
					fmt.Println(strings.TrimSpace(string(buffer)))
					// gotMsg = true
				}
			} else {
				// Create a buffer for the message data. This one is large enough
				// for the messages put by the amqsput sample.
				buffer := make([]byte, 1024)

				// Now we can try to get the message
				datalen, err = qObject.Get(getmqmd, gmo, buffer)

				if err != nil {
					msgAvail = false
					fmt.Println(err)
					mqret := err.(*ibmmq.MQReturn)
					if mqret.MQRC == ibmmq.MQRC_NO_MSG_AVAILABLE {
						// If there's no message available, then I won't treat that as a real error as
						// it's an expected situation
						rc = int(mqret.MQRC)
						err = nil
					}
				} else {
					// Assume the message is a printable string, which it will be
					// if it's been created by the amqsput program
					fmt.Printf("Got message of length %d: ", datalen)
					fmt.Println(strings.TrimSpace(string(buffer[:datalen])))
					// gotMsg = true
				}
			}
		}

		// Exit with any return code extracted from the failing MQI call.
		// Deferred disconnect will happen after the return
		if err != nil {
			mqret := int((err.(*ibmmq.MQReturn)).MQCC)
			fmt.Printf("mqret %d failed.\n", mqret)
		}
		qMgr.Disc() // Ignore errors from disconnect as we can't do much about it anyway
		fmt.Println("Done.")
	}
	os.Exit(rc)
}

// Close the queue if it was opened
func close(object ibmmq.MQObject) error {
	err := object.Close(0)
	if err == nil {
		fmt.Println("Closed queue")
	} else {
		fmt.Println(err)
	}
	return err
}
