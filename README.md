# MqLab
## Getting started

# Read about the mq container ibmcom/mq
https://github.com/ibm-messaging/mq-container/blob/master/docs/usage.md

# Mq-golang
https://github.com/ibm-messaging/mq-golang

# Another guide from IBM
https://community.ibm.com/community/user/integration/blogs/matt-roberts1/2021/03/12/mq-golangjms-openshift

## Quick description of the containers
ibm-mq9.2a          it's a standar QM 

ibm-mq9.2b          it's a standar QM 

mq_golang_client1  Golang client with JMS like syntax ( it connects to qm a )

mq_golang_client2  Golang client classical procedural style ( it connects to qm b )

they are all connected via mq-net network

## Create QM and Clients

```
git clone https://gitlab.com/Daide77/mqlab.git

# Create the dir for the qms' objects
cd mqlab
mkdir mqm && chmod o+xw mqm
mkdir mqm2 && chmod o+xw mqm2

echo "Create the local bind volumes for ssl keys"
MYQM=DAVEMQA
mkdir -p keys/${MYQM} && chmod o+xw keys/${MYQM} 
## Client Conf
MYCL=mq_golang_client1
mkdir keys/${MYCL} && chmod o+xw keys/${MYCL}

docker-compose build
docker-compose up

MYQMU=DAVEMQA
MYQM=$(echo "${MYQMU,,}")
MYCL=mq_golang_client1

## Create keystore on the a server
echo "docker exec  -it ibm-mq9.2a /bin/bash"
echo "cd /ssl/QM1"
echo "runmqakm -keydb -create -db ${MYQM}.kdb -pw Mypassword -stash"
## Check that the store is empty
echo "runmqakm -cert -list -db ${MYQM}.kdb -stashed"
## Create the server certificate and put it in the keystore
echo "runmqakm -cert -create -db ${MYQM}.kdb -stashed -dn \"cn=${MYQM},o=Dave,c=ch\" -label ibmwebspheremq${MYQM} -type cms"
## Check the certificate
echo "runmqakm -cert -list -db ${MYQM}.kdb -stashed" 
## Extract the publicKey
echo "runmqakm -cert -extract -label ibmwebspheremq${MYQM} -db ${MYQM}.kdb -stashed -file ${MYQM}.cert"
## Inspect the publicKey
echo "runmqakm -cert -details -file ${MYQM}.cert -stashed"
## Client Configuration
echo "cd /ssl/mq_golang_client1"
## Create keystore
MYCL=mq_golang_client1
echo "runmqakm -keydb -create -db ${MYCL}.kdb -pw Mypassword -stash"
echo "runmqakm -cert -create -db ${MYCL}.kdb -stashed -dn \"cn=${MYCL},o=Dave,c=ch\" -label ibmwebspheremq${MYCL} -type cms"
echo "runmqakm -cert -extract -label ibmwebspheremq${MYCL} -db ${MYCL}.kdb -stashed -file ${MYCL}.cert"
## Inspect the publicKey
echo "runmqakm -cert -details -file ${MYCL}.cert -stashed"

## Exchange public keys
## Add the pubblic key of server to the client
echo "runmqakm -cert -add -label ibmwebspheremq${MYQM} -db ${MYCL}.kdb -stashed -file  /ssl/QM1/${MYQM}.cert"
echo "runmqakm -cert -list -db ${MYCL}.kdb -stashed"

## Add the pubblic key of client to the server
echo "cd /ssl/QM1/"
## Add the pubblic key of client to the server
echo "runmqakm -cert -add -label ibmwebspheremq${MYCL} -db ${MYQM}.kdb -stashed -file  /ssl/mq_golang_client1/mq_golang_client1.cert"
echo "runmqakm -cert -list -db ${MYQM}.kdb -stashed"

echo "!!!!! Pay attentetion to the permission for the ssl file for the client and for the server"
# echo "# DEBUG 
# tail -f  /var/mqm/qmgrs/DAVEMQA/errors/AMQERR01.LOG"

echo "Add permissions for the client1"
echo "docker exec  -it ibm-mq9.2a /bin/bash
setmqaut -m DAVEMQA -n RQ.APP2 -t queue -p app  +put +get

docker exec  -it ibm-mq9.2b /bin/bash
setmqaut -m DAVEMQB -n RQ.APP1 -t queue -p app  +put +get"


```

## Goals

# Create QM and Clients  ( DONE )

# Implement client connection with different clients ( DONE )

# Inmplement QM channels ( DONE )

# Implement Certificate and encription ( DONE )

# Implement LDAP authetication ( TBD )

# Golang statically compiled client tools ( TBD )

## client1 connects to DAVEMQA

it gets from DEV.QUEUE.2 ( pointed from client2 via DAVEMQB )

it puts four messages to RQ.APP2 ( remote q for DEV.QUEUE.2 on DAVEMQB )

## client2 connects to DAVEMQB

it gets from DEV.QUEUE.2 ( pointed from client1 via DAVEMQA )

it puts one msg to RQ.APP1 ( remote q for DEV.QUEUE.2 on DAVEMQA )

```
```

